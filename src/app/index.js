import React from "react"
import { Provider } from "react-redux"
import { Router } from "react-router-dom"
import Routes from "./router"
import { store, history } from "./store"


class App extends React.Component {
    render() {
        return (
            <Provider store={store || {}}>
                <Router history={history}>
                    <Routes />
                </Router>
            </Provider>
        )
    }
}

export default App