import config from "../../../config"
import { tokenize, weatherApi } from "../../helpers/axios"

const weather = {
    state: {
    },
    reducers: {
        onError(state, data) {
            console.error(data)
            return {
                ...state,
                loading: false
            }
        },
        onRequest(state) {
            return {
                ...state,
                loading: true
            }
        },
        onGetWeatherSuccess(state, data) {
            return {
                ...state,
                loading: false,
                weather: data
            }
        }
    },
    effects: {
        async getWeather(payload, rootState) {
            this.onRequest()
            try {
                let res = await weatherApi()
                    .get(config.routes.getWeather, { params: tokenize(payload) })
                    .then(res => res.data)
                this.onGetWeatherSuccess(res)
                return res
            }
            catch (err) {
                this.onError(err)
            }
        }
    }
}

export default weather