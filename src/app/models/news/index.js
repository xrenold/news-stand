import config from "../../../config"
import { api } from "../../helpers/axios"

const news = {
    state: {
        
    },
    reducers: {
        onError(state, data) {
            console.error(data)
            return {
                ...state,
                loading: false 
            }
        },
        onRequest(state) {
            return {
                ...state,
                loading: true
            }
        },
        onGetHeadlinesSuccess(state, data) {
            return {
                ...state,
                loading: false,
                news: data
            }
        },
        onSearchNewsSuccess(state, data) {
            return {
                ...state,
                loading: false,
                news: data
            }
        }
    },
    effects: {
        async getHeadLines(payload, rootState) {
            this.onRequest()
            try {
                let res = await api()
                    .get(config.routes.getHeadlines, { params: payload})
                    .then(res => res.data)
                this.onGetHeadlinesSuccess(res)
                return res
            }
            catch(err) {
                this.onError(err)
            }
        },
        async searchNews(payload, rootState) {
            this.onRequest()
            try {
                let res = await api()
                    .get(config.routes.everything, { params: payload })
                    .then(res => res.data)
                this.onSearchNewsSuccess(res)
                return res
            }
            catch(err) {
                this.onError(err)
            }
        }
    }
}

export default news