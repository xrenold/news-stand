import axios from "axios";
import config from "../../config";

export function api() {
    let opts = {
        baseURL: config.api.trim(),
        headers: {
            "x-api-key": config.token
        },
    };
    return axios.create(opts);
}

export function weatherApi() {
    let opts = {
        baseURL: config.weatherApi.trim(),
        headers: {
            // "x-api-key": config.token
        },
    };
    return axios.create(opts);
}

export function tokenize(payload) {
    return {
        ...(payload || {}),
        appid: config.weatherKey
    }
}