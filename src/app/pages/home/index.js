import React from "react"
import { connect } from "react-redux"
import config from "../../../config"
import Loader from "../../components/loader"

const mapStateToProps = ({ news, weather }) => {
    return {
        ...news,
        ...weather
    }
}

const mapDispatchToProps = ({ news, weather }) => {
    return {
        ...news,
        ...weather
    }
}

class Home extends React.Component {

    state = {
        loading: true
    }

    componentDidMount() {
        this.init()
    }

    async init() {
        await Promise.all([
            new Promise(async (resolve, reject) => {
                await this.props.getHeadLines({
                    language: config.lang
                })
                resolve()
            }),
            new Promise(async (resolve, reject) => {
                let lat = 33.44, lng = -94.04
                if ("geolocation" in navigator) {
                    let position = await new Promise(function (resolve, reject) {
                        navigator.geolocation.getCurrentPosition(resolve, reject);
                    })
                    lat = Math.round(position.coords.latitude * 100) / 100
                    lng = Math.round(position.coords.longitude * 100) / 100

                } else {
                    console.log("Location Not Available, using fallback cordinates")
                }
                await this.props.getWeather({
                    "lat": lat,
                    "lon": lng,
                })
                resolve()
            })
        ])
        this.setState({ loading: false })
    }

    onCardClick(article) {
        if (article.url) window.open(article.url)
    }

    render() {
        const { weather } = this.props
        if (this.state.loading || this.props.loading) return <Loader />
        return (
            <div className="home-wrapper">
                <div className="weather-banner">
                    <div className="weather-cloud">
                        <img src="/images/cloud-removebg-preview.png" />
                        <div className="weather-details">
                            <span className="detail">{(weather && weather.name) || ""}: <strong>{(weather && weather.weather && weather.weather.length && weather.weather[0] && weather.weather[0].description) || "" }</strong></span>
                            <span className="detail">Temperature: <strong>{weather && weather.main && weather.main.temp}&#x2103;</strong></span>
                            <span className="detail">Humidity: <strong>{weather && weather.main && weather.main.humidity}</strong></span>
                        </div>
                    </div>
                </div>
                <div className="row card-wrapper">
                    {this.props.news && this.props.news.articles && this.props.news.articles.length ?
                        this.props.news.articles.map((article, index) => (
                            <div className="col-4 col-sm-6" key={index}>
                                <div className="card"
                                    onClick={() => this.onCardClick(article)}
                                >
                                    <div className="card-body">
                                        <figure className="image-wrapper">

                                            <img className="card-image" src={article.urlToImage || "/images/news.jpg"} alt="" />
                                            {article.description ?
                                                <p>
                                                    {article.description || ""}
                                                </p>
                                                : null}
                                        </figure>
                                    </div>
                                    <header className="card-header">
                                        {article.title || ""}
                                    </header>
                                </div>
                            </div>
                        ))
                        : null}
                </div>
            </div>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home)