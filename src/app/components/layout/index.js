import React from "react"
import Topbar from "../topbar"

class Layout extends React.Component {
    render() {
        return (
            <div className="layout">
                <Topbar />
                {this.props.children}
            </div>
        )
    }
}

export default Layout