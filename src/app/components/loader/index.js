import React from "react"
import Loading from 'react-loader-spinner'
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"

class Loader extends React.Component {
    render() {
        return (
            <div className="loading-wrapper">
                <Loading
                    type="BallTriangle"
                    color="#00BFFF"
                    height={100}
                    width={100}
                    // timeout={3000} //3 secs
                />
            </div>
        )
    }
}

export default Loader