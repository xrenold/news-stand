import React from "react"
import { SearchOutlined } from "@ant-design/icons"
import { connect } from "react-redux"
import config from "../../../config"
import Select from "react-select"


const mapStateToProps = ({ news }) => {
    return {
        ...news
    }
}

const mapDispatchToProps = ({ news }) => {
    return {
        ...news
    }
}

class Topbar extends React.Component {

    state = {
        search: "",
        lang: { value: "en", label: "en" }
    }

    onSearchEnter(event) {
        if (event.key === "Enter") {
            this.search()
        }
    }

    onLangChange(value) {
        this.setState({
            lang: value
        }, () => this.search())
    }

    search() {
        if (!this.state.search) {
            this.props.getHeadLines({
                language: this.state.lang.value
            })
        }
        else
            this.props.searchNews({
                q: this.state.search,
                language: this.state.lang.value
            })
    }

    render() {
        return (
            <header className="top-bar">
                <div className="top-bar-container">
                    {/* <div > */}
                    <img alt="" className="logo" src="https://i0.wp.com/blackpinkupdate.com/wp-content/uploads/2020/06/BLACKPINK-Logo-Pink-PNG.jpg?ssl=1" />
                    {/* </div> */}
                    <div className="search-input">
                        <input
                            placeholder="Search"
                            onChange={(event) => this.setState({ search: event.target.value })}
                            onKeyPress={this.onSearchEnter.bind(this)}
                        />
                        <button className="search-btn" onClick={() => this.search()}>
                            <SearchOutlined />
                        </button>
                        <div className="select-wrap">
                            <Select
                                placeholder="lang"
                                options={
                                    config.languages.map(item => (
                                        { value: item, label: item }
                                    ))
                                }
                                value={this.state.lang}
                                onChange={this.onLangChange.bind(this)}
                            />
                        </div>
                    </div>
                </div>
            </header>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Topbar)