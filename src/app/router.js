import React, { lazy, Suspense } from "react"
import Loader from "./components/loader"
import { Route, Switch } from "react-router-dom"
import Layout from "./components/layout"

const CustomRoute = ({ component: Component, layoutSettings = {}, ...rest }) => (
    <Route
        {...rest}
        render={props => {
            return (
                <Layout _settings={layoutSettings}>
                    <Component />
                </Layout>
            )
        }}
    />
)

export default function Router() {
    return (
        <Suspense fallback={<Loader />}>
            <Switch>
                <CustomRoute
                    path="/"
                    exact
                    component={lazy(() => import("./pages/home"))}
                />
            </Switch>
        </Suspense>
    )
}