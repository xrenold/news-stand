const serverPath = {
    development: "https://newsapi.org/v2/",
    staging: "https://newsapi.org/v2/",
    production: "https://newsapi.org/v2/"
}

const weatherServerPath = {
    development: "https://api.openweathermap.org/data/2.5",
    staging: "https://api.openweathermap.org/data/2.5",
    production: "https://api.openweathermap.org/data/2.5"
}

const appEnvironment = process.env.REACT_APP_ENV || "development";

let server = serverPath[appEnvironment];
let weatherServer = weatherServerPath[appEnvironment]


const env = {
    development: {
        token: "c797f428e52946f88bb75a9f31716c99",
        weatherKey: "dd07ba87485731635eba4a9c6bb00ce0"
    },
    staging: {
        token: "c797f428e52946f88bb75a9f31716c99",
        weatherKey: "dd07ba87485731635eba4a9c6bb00ce0"
    },
    production: {
        token: "c797f428e52946f88bb75a9f31716c99",
        weatherKey: "dd07ba87485731635eba4a9c6bb00ce0"
    }
}

let config = {
    api: server,
    weatherApi: weatherServer,
    lang: "en",
    ...env[appEnvironment],
    languages: [
        "ar", "de", "en", "es", "fr", "he", "it", "nl", "no", "pt", "ru", "se", "ud", "zh"
    ],
    routes: {
        getHeadlines: "/top-headlines",
        everything: "/everything",
        getWeather: "/weather"
    }
}

export default config